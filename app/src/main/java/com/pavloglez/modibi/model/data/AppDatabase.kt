package com.pavloglez.modibi.model.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.pavloglez.modibi.model.data.daos.MovieDao
import com.pavloglez.modibi.utils.Constants.DATABASE_NAME

@Database(entities = [Movie::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun moviesDao(): MovieDao

    companion object {
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                Room.databaseBuilder(
                    context,
                    AppDatabase::class.java, DATABASE_NAME
                ).build()
            }
        }
    }
}