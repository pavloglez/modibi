package com.pavloglez.modibi.model.data.repositories

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.pavloglez.modibi.model.data.AppDatabase
import com.pavloglez.modibi.model.data.Movie
import com.pavloglez.modibi.model.data.MovieList
import com.pavloglez.modibi.model.data.Resource
import com.pavloglez.modibi.model.network.MovieApi
import javax.inject.Inject

class MovieRepository @Inject constructor(private val api: MovieApi, private val application: Application) {

    private val database = AppDatabase.getInstance(application)
    private val movieDao = database.moviesDao()

    private val _moviesResource = MutableLiveData<Resource<MovieList>>()
    val movieResource: LiveData<Resource<MovieList>>
        get() = _moviesResource

    suspend fun fetchMoviesListWrapper() {
        _moviesResource.value = Resource.loading(null)
        try {
            _moviesResource.value = Resource.success(api.getMoviesList())
        } catch (throwable: Throwable) {
            _moviesResource.value = Resource.error("", null)
        }
    }

    //database queries
    suspend fun getStoredMovies(): List<Movie>? {
        return movieDao.getAllMovies()
    }

    suspend fun getMovie(movieId: Int): Movie? {
        return movieDao.getMovie(movieId)
    }

    suspend fun deleteMovies() {
        movieDao.deleteAllMovies()
    }

    suspend fun insertMovies(movies: List<Movie>) {
        movieDao.insertMovies(movies)
    }

}

