package com.pavloglez.modibi.model.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
data class Movie (
    @PrimaryKey(autoGenerate = true) var tableId: Long?,
    @ColumnInfo(name = "id") var id: Int?,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "overview") var overview: String?,
    @ColumnInfo(name = "poster") var poster: String?,
    @ColumnInfo(name = "rating") var rating: Double?,
    @ColumnInfo(name = "popularity") var popularity: Double?
    ) {
    constructor() : this (0,0, "", "", "", 0.0, 0.0)

    constructor(movie: Result) : this(
        null,
        movie.id,
        movie.title,
        movie.overview,
        movie.poster_path,
        movie.vote_average,
        movie.popularity
    )
}