package com.pavloglez.modibi.model.data

class MovieList(
    var average_rating: Double?,
    var backdrop_path: String?,
    var created_by: CreatedBy?,
    var description: String?,
    var id: Int?,
    var iso_3166_1: String?,
    var iso_639_1: String?,
    var name: String?,
    var page: Int?,
    var poster_path: String?,
    var public: Boolean?,
    var results: List<Result>?,
    var revenue: Long?,
    var runtime: Int?,
    var sort_by: String?,
    var total_pages: Int?,
    var total_results: Int?
)

data class CreatedBy(
    var gravatar_hash: String?,
    var id: String?,
    var name: String?,
    var username: String?
)

data class Result(
    var adult: Boolean?,
    var backdrop_path: String?,
    var genre_ids: List<Int>?,
    var id: Int?,
    var media_type: String?,
    var original_language: String?,
    var original_title: String?,
    var overview: String?,
    var popularity: Double?,
    var poster_path: String?,
    var release_date: String?,
    var title: String?,
    var video: Boolean?,
    var vote_average: Double?,
    var vote_count: Int?
)