package com.pavloglez.modibi.model.network

import com.pavloglez.modibi.model.data.MovieList
import com.pavloglez.modibi.utils.Constants.API_KEY
import com.pavloglez.modibi.utils.Constants.GET_MOVIES_LIST
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {
    @GET(GET_MOVIES_LIST)
    suspend fun getMoviesList(
        @Query("page") page: Int = 1,
        @Query("api_key") apiKey: String = API_KEY
    ): MovieList
}