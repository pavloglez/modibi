package com.pavloglez.modibi.di.components

import android.app.Application
import com.pavloglez.modibi.di.modules.network.NetWorkModule
import com.pavloglez.modibi.di.modules.viewModule.ViewModelModule
import com.pavloglez.modibi.ui.movieDetails.MovieDetailsFragment
import com.pavloglez.modibi.ui.movielist.MovieListFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetWorkModule::class, ViewModelModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(movieListFragment: MovieListFragment)
    fun inject(movieListFragment: MovieDetailsFragment)
}