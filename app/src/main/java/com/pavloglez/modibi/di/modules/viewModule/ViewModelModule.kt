package com.pavloglez.modibi.di.modules.viewModule

import androidx.lifecycle.ViewModel
import com.pavloglez.modibi.ui.movieDetails.MovieDetailsViewModel
import com.pavloglez.modibi.ui.movielist.MovieListViewModel
import com.pavloglez.modibi.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MovieListViewModel::class)
    internal abstract fun bindMovieListViewModel(viewModel: MovieListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailsViewModel::class)
    internal abstract fun bindMovieDetailsViewModel(viewModel: MovieDetailsViewModel): ViewModel
}