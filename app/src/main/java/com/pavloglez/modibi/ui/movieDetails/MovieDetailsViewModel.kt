package com.pavloglez.modibi.ui.movieDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pavloglez.modibi.model.data.Movie
import com.pavloglez.modibi.model.data.repositories.MovieRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieDetailsViewModel @Inject constructor(private val repository: MovieRepository) : ViewModel() {
    private val _movie = MutableLiveData<Movie>()
    val movie: LiveData<Movie>
        get() = _movie

    fun getMovie(movieId: Int) {
        viewModelScope.launch {
            _movie.value = repository.getMovie(movieId)
        }
    }
}