package com.pavloglez.modibi.ui.movielist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.pavloglez.modibi.R
import com.pavloglez.modibi.base.BaseApplication
import com.pavloglez.modibi.databinding.MovieListFragmentBinding
import com.pavloglez.modibi.di.modules.viewModule.ViewModelFactory
import com.pavloglez.modibi.model.data.Movie
import com.pavloglez.modibi.utils.adapters.MoviesAdapter
import com.pavloglez.modibi.utils.adapters.SearchType
import com.pavloglez.modibi.utils.goToTop
import javax.inject.Inject

class MovieListFragment: Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: MovieListViewModel

    private lateinit var binding: MovieListFragmentBinding

    private lateinit var moviesAdapter: MoviesAdapter

    private val movieSelectionListener = object : MoviesAdapter.MovieSelectionListener {
        override fun onTap(movieId: Int) {
            NavHostFragment.findNavController(this@MovieListFragment).navigate(
                MovieListFragmentDirections.actionListToDetails().apply {
                    this.movieId = movieId
                }
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        BaseApplication.appComponent.inject(this)
        binding = DataBindingUtil.inflate(inflater, R.layout.movie_list_fragment, container, false)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MovieListViewModel::class.java)
        setupFilterButtons()
        loadMovies()
        return binding.root
    }

    private fun setupFilterButtons() {
        binding.run {
            filterByPopularity.run {
                isEnabled = true
                setOnClickListener {
                    moviesRecycler.goToTop()
                    moviesAdapter.sortMoviesBy(SearchType.POPULARITY)
                }
            }

            filterByRating.run {
                isEnabled = true
                setOnClickListener {
                    moviesRecycler.goToTop()
                    moviesAdapter.sortMoviesBy(SearchType.RATING)
                }
            }
        }
    }

    private fun loadMovies() {
        viewModel.run {
            getMoviesFromDB()
            storedMovies.observe(viewLifecycleOwner, { movies ->
                binding.run {
                    moviesRecycler.run {
                        layoutManager = GridLayoutManager(context, 2)
                        moviesAdapter = MoviesAdapter(movies, movieSelectionListener)
                        adapter = moviesAdapter
                        setupFilterButtons()
                    }

                    movieSearchTerm.text?.let {
                        if (it.isNotBlank()) filterMovies(viewModel.storedMovies.value, it.toString())
                    }

                    swiperefresh.run {
                        isRefreshing = false
                        setOnRefreshListener {
                            movieSearchTerm.setText("")
                            viewModel.getMoviesWrapped()
                            isRefreshing = true
                        }
                    }

                    movieSearchTerm.run {
                        doAfterTextChanged {
                            filterMovies(movies, it.toString())
                        }
                    }
                }
            })
            errorMsg.observe(viewLifecycleOwner, {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            })

            isLoading.observe(viewLifecycleOwner, {
                if (it) Toast.makeText(context, "Loading...", Toast.LENGTH_SHORT).show()
            })
        }
    }

    private fun filterMovies(movies: List<Movie>?, searchTerm: String) {
        movies?.let {
            moviesAdapter.setMovies(it.filter { movie ->
                movie.title?.contains(searchTerm, true) ?: false
            })
        }

    }
}