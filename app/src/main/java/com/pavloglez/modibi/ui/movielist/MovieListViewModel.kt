package com.pavloglez.modibi.ui.movielist

import androidx.lifecycle.*
import com.pavloglez.modibi.model.data.Movie
import com.pavloglez.modibi.model.data.MovieList
import com.pavloglez.modibi.model.data.Resource
import com.pavloglez.modibi.model.data.Status
import com.pavloglez.modibi.model.data.repositories.MovieRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieListViewModel @Inject constructor(private val repository: MovieRepository) : ViewModel() {

    private val _moviesList = MutableLiveData<MovieList>()
    val movieList: LiveData<MovieList>
        get() = _moviesList

    private val _storedMovies = MutableLiveData<List<Movie>>()
    val storedMovies: LiveData<List<Movie>>
        get() = _storedMovies

    private val _errorMsg = MutableLiveData<String>()
    val errorMsg: LiveData<String>
        get() = _errorMsg

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val moviesObserver =
        Observer<Resource<MovieList>> { response ->
            when (response?.status) {
                Status.LOADING -> {
                    _isLoading.value = true
                }
                Status.SUCCESS -> {
                    _isLoading.value = false
                    _moviesList.value = response.data
                    deleteMoviesFromDB()
                }
                Status.ERROR -> {
                    _isLoading.value = false
                    _errorMsg.value = "Something went wrong :C, fetching movies from local DB"
                    getMoviesFromDB()
                }
            }
        }

    override fun onCleared() {
        repository.movieResource.removeObserver(moviesObserver)
        super.onCleared()
    }

    fun getMoviesWrapped() {
        viewModelScope.launch {
            repository.run {
                fetchMoviesListWrapper()
                movieResource.observeForever(moviesObserver)
            }
        }
    }

    fun getMoviesFromDB() {
        viewModelScope.launch {
            _storedMovies.value = repository.getStoredMovies()
        }.invokeOnCompletion {
            if (storedMovies.value?.isEmpty() == true) getMoviesWrapped()
        }
    }

    private fun deleteMoviesFromDB() {
        viewModelScope.launch {
            repository.deleteMovies()
        }.invokeOnCompletion {
            if (movieList.value?.results?.isNotEmpty() == true) insertMovies()
        }
    }

    private fun insertMovies() {
        viewModelScope.launch {
            val movieDbList: MutableList<Movie> = mutableListOf()
            movieList.value?.results?.forEach {
                movieDbList.add(
                    Movie(it)
                )
            }
            _storedMovies.value = movieDbList
            repository.insertMovies(movieDbList)
        }
    }

}