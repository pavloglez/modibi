package com.pavloglez.modibi.ui.movieDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.pavloglez.modibi.R
import com.pavloglez.modibi.base.BaseApplication
import com.pavloglez.modibi.databinding.MovieDetailFragmentBinding
import com.pavloglez.modibi.di.modules.viewModule.ViewModelFactory
import javax.inject.Inject

class MovieDetailsFragment: Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: MovieDetailFragmentBinding

    private lateinit var viewModel: MovieDetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        BaseApplication.appComponent.inject(this)
        binding = DataBindingUtil.inflate(inflater, R.layout.movie_detail_fragment, container, false)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MovieDetailsViewModel::class.java)
        getMovieDetails()
        return binding.root
    }

    private fun getMovieDetails() {
        viewModel.run {
            getMovie(MovieDetailsFragmentArgs.fromBundle(requireArguments()).movieId)
            movie.observe(viewLifecycleOwner, { movie ->
                binding.movie = movie
            })
        }
    }
}