package com.pavloglez.modibi.utils.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.pavloglez.modibi.R
import com.pavloglez.modibi.utils.Constants.IMAGES_PATH

@BindingAdapter("imageUrl")
fun bindImage(imageView: ImageView, imgUrl: String?) {
    Glide.with(imageView.context)
        .load("$IMAGES_PATH$imgUrl")
        .apply(
            RequestOptions()
                .error(R.drawable.broken_image)
        )
        .into(imageView)
}