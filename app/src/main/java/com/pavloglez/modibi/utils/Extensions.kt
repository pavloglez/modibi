package com.pavloglez.modibi.utils

import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.goToTop() {
    scrollToPosition(0)
}