package com.pavloglez.modibi.utils

object Constants {
    const val BASE_URL = "https://api.themoviedb.org"
    const val API_KEY = "{YOUR_API_KEY}"
    const val GET_MOVIES_LIST = "/4/list/1"
    const val IMAGES_PATH = "https://image.tmdb.org/t/p/w500/"
    const val DATABASE_NAME = "modibi-db"
}