package com.pavloglez.modibi.utils.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pavloglez.modibi.databinding.MovieListItemBinding
import com.pavloglez.modibi.model.data.Movie
import java.util.Collections

class MoviesAdapter(private var movies: List<Movie>, private val onMovieSelectedListener: MovieSelectionListener) : RecyclerView.Adapter<MovieViewHolder>() {

    interface MovieSelectionListener {
        fun onTap(movieId: Int)
    }

    fun sortMoviesBy(type: SearchType) {
        Collections.sort(movies) { p0, p1 ->
            when(type) {
                SearchType.RATING -> {
                    p0?.rating?.compareTo(p1?.rating ?: 0.0) ?: 1
                }
                SearchType.POPULARITY -> {
                    p0?.popularity?.compareTo(p1?.popularity ?: 0.0) ?: 1
                }
            }
        }
        notifyDataSetChanged()
    }

    fun setMovies(newMovies: List<Movie>) {
        movies = newMovies
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(MovieListItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        movies[position].let { movie ->
            holder.bind(movie)
            holder.itemView.setOnClickListener {
                movie.id?.let {
                    onMovieSelectedListener.onTap(it)
                }
            }
        }
    }

    override fun getItemCount(): Int = movies.size
}

class MovieViewHolder(private val binding: MovieListItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(movie: Movie) {
        binding.run {
            this.movie = movie
        }
    }
}

enum class SearchType {
    RATING, POPULARITY
}