package com.pavloglez.modibi.base

import android.app.Application
import com.pavloglez.modibi.di.components.AppComponent
import com.pavloglez.modibi.di.components.DaggerAppComponent

class BaseApplication : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().application(this).build()
    }
}