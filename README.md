## MoDibi

This is a very simple application that displays https://www.themoviedb.org/ API information as demonstration purposes, made using:

* Kotlin
* MVVM
* Coroutines
* Retrofit
* OkHttp
* Dagger2 as dependency injection
* Glide to load images
* LiveData
* Navigation
* Room

Missing features(TODO):
If I had the chance at the spected timeframe (I had plans for the weekend before knowing I had to work on this) I would like to have implemented the following features:

* Proper request state handling (showing the user we're requesting the information, error handling, etc)
* Unit testing
* UI testing
* More details into the Movie details section
* A more polished UI
* Trailer player in movie details ( I must admit that maybe I didn't take a proper look into the movie db API responses, but I didn't see any video URL in the movie details)
* If movie URL is not present in the API response, maybe add the Youtube Player API to search for the movie's trailer
